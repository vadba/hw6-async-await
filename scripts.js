// Теоретичне питання

// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Виконання декількох завдань одночасно. Тобто, це механізм, за допомогою якого JavaScript імітує паралельну роботу. В потрібний момент розміщується список дій, що очікували виконання, в стек викликів при його звільненні.

// Завдання

document.querySelector('.btn').addEventListener('click', async e => {
    const content = document.querySelector('.display');
    content.classList.remove('loaded');

    const response = await fetch('http://api.ipify.org/?format=json');
    const ipMy = await response.json();

    const responseDirection = await fetch(`http://ip-api.com/json/${ipMy.ip}?fields=continent,country,regionName,city,district,query`);
    const direction = await responseDirection.json();

    document.querySelector('.content').innerHTML = `
        <ul class="list-group">
            <li class="list-group-item list-group-item-primary">Kонтинент -> ${direction.continent}</li>
            <li class="list-group-item list-group-item-primary">Kраїна -> ${direction.country}</li>
            <li class="list-group-item list-group-item-primary">Pегіон -> ${direction.regionName}</li>
            <li class="list-group-item list-group-item-primary">Mісто -> ${direction.city}</li>
            <li class="list-group-item list-group-item-primary">Pайон -> ${direction.district}</li>
        </ul>`;

    content.classList.add('loaded_hiding');

    window.setTimeout(function () {
        content.classList.add('loaded');
        content.classList.remove('loaded_hiding');
    }, 300);
});
